# CS-Cinvestav-Exam-Generator

This project aims to build a computer program that builds bank of questions files for the construction of admission exams of the [Computer Science Department][12] of [Cinvestav][11], which takes as input files of datasets of questions and selecting them randomly.

## Content

- [Usage](#usage)
    - [Get a copy](#get-a-copy)
    - [Examples](#examples)
- [Development](#development)
    - [Contribuiting](#contributing)
- [Authors](#authors)

## Usage

Follow these steps:

1.  Get a copy of source code.
2.  Compile project.
3.  Use it.

This project does not require (and does not support yet) installation process. This does not imply that you can put it _manually_ into a directory pointed by [`PATH` variable][14].

### Get a copy

You can get a copy using `git` command or by downloading a compressed file.

1.  Clone this git project by executing this command:
    ```bash
    git clone https://gitlab.com/israelbuitron/cs-cinvestav-exam-generator.git
    ```
2.  Download a compressed file of [`master`][5] branch (or any branch available) of this project from its [git repository][6], in one of these formats: [zip][1], [tar.gz][2], [tar.bz2][3] or [tar][4].

### Compile project

1.  Open a terminal.
2.  Go to project root directory.
3.  Run `make gen`. This should generates an executable file called `gen`.

### Execute it

1.  Open a terminal.
2.  Go to project root directory. You should see its content similary to:
    ```bash
    ll
    total 96
    -rw-r--r--  1 ibuitron  users   181B Apr 17 00:32 Makefile
    -rw-r--r--  1 ibuitron  users   3.1K Apr 18 13:26 README.md
    -rwxr-xr-x  1 ibuitron  users    14K Apr 18 15:53 gen
    -rw-r--r--  1 ibuitron  users   5.7K Apr 17 00:32 gen.c
    -rw-------  1 ibuitron  users    96B Apr 14 17:35 gen.h
    -rw-r--r--  1 ibuitron  users   365B Apr 17 00:32 log.c
    -rw-r--r--  1 ibuitron  users   141B Apr 17 00:32 log.h
    -rw-r--r--  1 ibuitron  users   3.9K Apr 18 15:53 log.o
    ```
3.  Run `./gen` command.
    It takes these command-line arguments:
    - `-n`, positive integer number of questions select from raffle. It is mandatory.
    - `-i`, filename of Tex input file. This is the source from `gen` will read questions to raffle. It is mandatory.
    - `-o`, filename of Tex output file. To this file `gen` will store questions selected. It is mandatory.
    - `-v`, verbose flag. It is optional.

### Examples

You can go to [`examples`][10] directory and look into [`generate-exam.sh`][9] shell script file.
Here you have a sample [video][13] of usage.

## Development

The input of this program is LaTeX files as datasets of questions.
These questions have this structure:

```TeX
\cuen{prob}  Is this a dummy question?

\begin{enumerate}
\item Yes, it is.
\item No, it is not.
\end{enumerate}
\begin{flushright}
{\bf Author: Just-a-guy. Answer:} a
\end{flushright}
```

### Contributing

Bug reports and pull requests are welcome in our [repository][6]. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant][7] code of conduct.

## Authors

* **Israel Buitr&oacute;n** - *PhD Student at Cinvestav* - [Webpage][8] - ibuitron@computacion.cs.cinvestav.mx

[1]: https://gitlab.com/israelbuitron/cs-cinvestav-exam-generator/-/archive/master/cs-cinvestav-exam-generator-master.zip
[2]: https://gitlab.com/israelbuitron/cs-cinvestav-exam-generator/-/archive/master/cs-cinvestav-exam-generator-master.tar.gz
[3]: https://gitlab.com/israelbuitron/cs-cinvestav-exam-generator/-/archive/master/cs-cinvestav-exam-generator-master.tar.bz2
[4]: https://gitlab.com/israelbuitron/cs-cinvestav-exam-generator/-/archive/master/cs-cinvestav-exam-generator-master.tar
[5]: https://gitlab.com/israelbuitron/cs-cinvestav-exam-generator/tree/master
[6]: https://gitlab.com/israelbuitron/cs-cinvestav-exam-generator
[7]: http://contributor-covenant.org
[8]: http://computacion.cs.cinvestav.mx/~ibuitron
[9]: https://gitlab.com/israelbuitron/cs-cinvestav-exam-generator/blob/master/examples/generate-exam.sh
[10]: https://gitlab.com/israelbuitron/cs-cinvestav-exam-generator/blob/master/examples
[11]: http://www.cs.cinvestav.mx/
[12]: http://www.cinvestav.mx/
[13]: https://www.youtube.com/watch?v=nBMvjhvGEc8
[14]: https://en.wikipedia.org/wiki/PATH_(variable)
